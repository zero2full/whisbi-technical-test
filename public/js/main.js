let currencyRateInfoFromSelectedDate;
let currencyRateInfoFromOneDayBeforeSelectedDate;
let currencyArry;
let filteredCurrencyArry;
let selectedBase = 'USD';
let today = getDateString();
let selectedDate = getDateString();
let oneDayBefore = getOneDayBeforeDateString();
const formEl = document.getElementById('form');
const currencyEl = document.getElementById('currency');
const dateEl = document.getElementById('date');
const filterEl = document.getElementById('filter');
const dataContainerEl = document.getElementById('data-container');
const errorContainerEl = document.getElementById('error-container');
const exportEl = document.getElementById('export');

// get date string for api request
function getDateString(date = new Date()) {
  let month;
  if (date.getMonth() <= 8) {
    month = '0' + (date.getMonth() + 1);
  } else {
    month = date.getMonth() + 1;
  }

  return date.getFullYear() + '-' + month + '-' + date.getDate();
}

// get one day before string for api request
function getOneDayBeforeDateString(date = new Date()) {
  const millisecondsPerDay = 24 * 60 * 60 * 1000;
  const millisecondsFromDate = date.getTime();
  const millisecondsFromOneDayBefore =
    millisecondsFromDate - millisecondsPerDay;
  return getDateString(new Date(millisecondsFromOneDayBefore));
}

// convert input date to accurate date with time, because in the form, date from input only includes month, day, year, but not time, and it is not accurate
function convertInputDate(inputDate) {
  const currentDateStringArr = new Date().toLocaleString().split('/');
  const inputDateStringArr = inputDate.split('-');
  currentDateStringArr[0] = inputDateStringArr[1];
  currentDateStringArr[1] = inputDateStringArr[2];
  const tempArrToReplaceYear = currentDateStringArr[2].split(',');
  tempArrToReplaceYear[0] = inputDateStringArr[0];
  currentDateStringArr[2] = tempArrToReplaceYear.join(',');

  return currentDateStringArr.join('/');
}

// set up http request
function sendHttpRequest(date) {
  const promise = new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    if (date === today) {
      xhr.open(
        'GET',
        `https://api.ratesapi.io/api/latest?base=${selectedBase}`
      );
    } else if (date !== today) {
      xhr.open(
        'GET',
        `https://api.ratesapi.io/api/${date}?base=${selectedBase}`
      );
    }
    xhr.responseType = 'json';
    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(xhr.response);
      } else {
        resolve(xhr.response);
      }
    };
    xhr.onerror = () => {
      reject({ error: 'Something went wrong!' });
    };
    xhr.send();
  });
  return promise;
}

// get the api data to initial page load
function fetchApiData() {
  sendHttpRequest(selectedDate)
    .then((responseData) => {
      currencyRateInfoFromSelectedDate = responseData;
      currencyArry = createSortedCurrencyName(currencyRateInfoFromSelectedDate);
      return sendHttpRequest(oneDayBefore);
    })
    .then((responseData) => {
      errorContainerEl.classList.remove('error');
      exportEl.disabled = false;
      currencyRateInfoFromOneDayBeforeSelectedDate = responseData;
      createTable(
        currencyRateInfoFromOneDayBeforeSelectedDate.date,
        currencyRateInfoFromSelectedDate.date,
        currencyArry
      );
    })
    .catch((err) => {
      errorContainerEl.classList.add('error');
      errorContainerEl.innerHTML = err.error;
      dataContainerEl.innerHTML = '';
      exportEl.disabled = true;
    });
}

// get api data based on user input
function getCurrencyRates(event) {
  const inputDate = convertInputDate(dateEl.value);
  event.preventDefault();
  selectedBase = currencyEl.value;
  selectedDate = getDateString(new Date(inputDate));

  oneDayBefore = getOneDayBeforeDateString(new Date(inputDate));

  fetchApiData();
}

// create sorted currency array and assign it to currency property
function createSortedCurrencyName(ratesObj) {
  let currencyNameArr = Object.keys(ratesObj.rates);
  currencyNameArr.sort();
  currencyNameArr = currencyNameArr.filter(
    (currency) => currency !== selectedBase
  );
  currencyNameArr.unshift(selectedBase);
  return currencyNameArr;

  // create currency array using for render data in the table
  // ratesObj.currency = currencyNameArr;
}

// generate table with data
function createTable(dateBefore, dateAfter, currencyArr) {
  const currentTableEl = document.querySelector('table');
  if (currentTableEl) {
    currentTableEl.remove();
  }
  if (currencyArr.length !== 0) {
    // const currencyArr = currencyRateInfoFromSelectedDate.currency;
    const tableEl = document.createElement('table');

    // generate the whole table header
    const tableTheadEl = document.createElement('thead');
    const tableHeaderRowEl = document.createElement('tr');
    // currency header column
    const tableHeaderEl_1 = document.createElement('th');
    tableHeaderEl_1.appendChild(document.createTextNode('Currency'));
    tableHeaderRowEl.appendChild(tableHeaderEl_1);
    // one day before header column
    const tableHeaderEl_2 = document.createElement('th');
    tableHeaderEl_2.appendChild(document.createTextNode(dateBefore));
    tableHeaderRowEl.appendChild(tableHeaderEl_2);
    // selected date header column
    const tableHeaderEl_3 = document.createElement('th');
    tableHeaderEl_3.appendChild(document.createTextNode(dateAfter));
    tableHeaderRowEl.appendChild(tableHeaderEl_3);
    // append to table
    tableTheadEl.appendChild(tableHeaderRowEl);
    tableEl.appendChild(tableTheadEl);

    //generate the whole table body
    const tableTbodyEl = document.createElement('tbody');

    // create first base currency row
    const tableBodyRowEl = document.createElement('tr');
    const tableCellEl_1 = document.createElement('td');
    tableCellEl_1.appendChild(document.createTextNode(currencyArr[0]));
    tableBodyRowEl.appendChild(tableCellEl_1);
    const tableCellEl_2 = document.createElement('td');
    tableCellEl_2.appendChild(document.createTextNode(1));
    tableBodyRowEl.appendChild(tableCellEl_2);
    const tableCellEl_3 = document.createElement('td');
    tableCellEl_3.appendChild(document.createTextNode(1));
    tableBodyRowEl.appendChild(tableCellEl_3);
    tableTbodyEl.appendChild(tableBodyRowEl);

    // create other rows
    for (let i = 1; i < currencyArr.length; i++) {
      const rateBefore =
        currencyRateInfoFromOneDayBeforeSelectedDate.rates[currencyArr[i]];
      const rateAfter = currencyRateInfoFromSelectedDate.rates[currencyArr[i]];
      let setClass = '';
      if (rateAfter !== rateBefore) {
        setClass = rateAfter > rateBefore ? 'rate-up' : 'rate-down';
      }

      const tableBodyRowEl = document.createElement('tr');
      // currency column
      const tableCellEl_1 = document.createElement('td');
      tableCellEl_1.appendChild(document.createTextNode(currencyArr[i]));
      tableBodyRowEl.appendChild(tableCellEl_1);
      // one day before currency rates
      const tableCellEl_2 = document.createElement('td');
      tableCellEl_2.setAttribute('class', setClass);
      tableCellEl_2.appendChild(document.createTextNode(rateBefore));
      tableBodyRowEl.appendChild(tableCellEl_2);
      // select currency rates
      const tableCellEl_3 = document.createElement('td');
      tableCellEl_3.setAttribute('class', setClass);
      if (rateAfter < rateBefore) {
        tableCellEl_3.innerHTML = '&#8595; ';
      } else if (rateAfter > rateBefore) {
        tableCellEl_3.innerHTML = '&#8593; ';
      }

      tableCellEl_3.appendChild(document.createTextNode(rateAfter));
      tableBodyRowEl.appendChild(tableCellEl_3);
      // append to tbody
      tableTbodyEl.appendChild(tableBodyRowEl);
    }

    tableEl.appendChild(tableTbodyEl);
    dataContainerEl.appendChild(tableEl);
  }
}

// filter currency user inputs
function filterCurrency() {
  if (currencyArry.includes(selectedBase)) {
    const filterValue = filterEl.value.toUpperCase();
    if (Array.from(errorContainerEl.classList).includes('error')) {
      errorContainerEl.classList.remove('error');
    }
    if (exportEl.hasAttribute('disabled')) {
      exportEl.disabled = false;
    }
    filteredCurrencyArry = currencyArry.filter(
      (c) => c.indexOf(filterValue) === 0
    );
    if (
      filteredCurrencyArry.length !== currencyArry.length &&
      filteredCurrencyArry.length >= 1
    ) {
      filteredCurrencyArry.unshift(selectedBase);
    }
    if (filteredCurrencyArry.length === 0) {
      errorContainerEl.classList.add('error');
      exportEl.disabled = true;
      errorContainerEl.innerHTML = `"${filterValue}" is not available!`;
    }

    createTable(
      currencyRateInfoFromOneDayBeforeSelectedDate.date,
      currencyRateInfoFromSelectedDate.date,
      filteredCurrencyArry
    );
  }
}

// export table to excel
function fnExcelReport() {
  let i, j;
  let csv = '';

  const tableEl = document.getElementsByTagName('table')[0];

  var table_headings = tableEl.children[0].children[0].children;
  var table_body_rows = tableEl.children[1].children;

  let heading;
  const headingsArray = [];
  for (i = 0; i < table_headings.length; i++) {
    heading = table_headings[i];
    headingsArray.push('"' + heading.innerHTML + '"');
  }

  csv += headingsArray.join(',') + '\n';

  let row;
  let columns;
  let column;
  let columnsArray;
  for (i = 0; i < table_body_rows.length; i++) {
    row = table_body_rows[i];
    columns = row.children;
    columnsArray = [];
    for (j = 0; j < columns.length; j++) {
      column = columns[j];
      columnsArray.push('"' + column.innerHTML + '"');
    }
    csv += columnsArray.join(',') + '\n';
  }

  download('export.csv', csv);
}

// set download trigger
function download(filename, text) {
  const pom = document.createElement('a');
  pom.setAttribute(
    'href',
    'data:text/csv;charset=utf-8,' + encodeURIComponent(text)
  );
  pom.setAttribute('download', filename);

  if (document.createEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  } else {
    pom.click();
  }
}

// initiate first page loading
function setupPage() {
  dateEl.value = getDateString();
  if (formEl.addEventListener) {
    formEl.addEventListener('submit', getCurrencyRates, false);
    filterEl.addEventListener('keyup', filterCurrency, false);
    exportEl.addEventListener('click', fnExcelReport, false);
  } else if (formEl.attachEvent) {
    formEl.attachEvent('onsubmit', getCurrencyRates);
    filterEl.attachEvent('onkeyup', filterCurrency);
    exportEl.attachEvent('onclick', fnExcelReport);
  }
  // fetch data to initiate page
  fetchApiData();
}

// using load events to set up page loading
if (window.addEventListener) {
  window.addEventListener('load', setupPage, false);
} else if (window.attachEvent) {
  window.attachEvent('onload', setupPage);
}
